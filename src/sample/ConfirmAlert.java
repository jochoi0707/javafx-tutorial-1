package sample;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ConfirmAlert {
    static boolean signoutChoice = false;

    public static boolean display(String title, String subtitle) {
        Stage window = new Stage();
        // this will block the user from triggering other events
        // until this window is taken care of or dismissed
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(title);
        window.setMinWidth(200);
        window.setMinHeight(200);

        Label signoutAlertLabel = new Label();
        signoutAlertLabel.setText(title);

        Label signoutAlertSubLabel = new Label();
        signoutAlertSubLabel.setText(subtitle);

        Button cancelButton = new Button();
        cancelButton.setText("Cancel");
        cancelButton.setOnAction(e -> window.close());

        Button confirmButton = new Button();
        confirmButton.setText("Confirm");
        confirmButton.setOnAction(e -> {
            System.out.println("Confirm button for sign out was clicked!");
            signoutChoice = true;
            window.close();
        });

        HBox buttonContainer = new HBox();
        buttonContainer.setSpacing(10);
        buttonContainer.getChildren().addAll(cancelButton, confirmButton);

        VBox alertContainer = new VBox();
        alertContainer.setSpacing(20);
        alertContainer.getChildren().addAll(signoutAlertLabel, signoutAlertSubLabel, buttonContainer);
        alertContainer.setAlignment(Pos.CENTER);

        Scene signoutAlertScene = new Scene(alertContainer);
        window.setScene(signoutAlertScene);
        window.showAndWait();
        return signoutChoice;
    }
}
