package sample;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

// all the core functionality of a javafx application is wrapped up in the
// application class, that is why we need to 'extends' Application for any
// new javafx application
// whenever you want to handle events, need to implement the EventHandler interface
public class Main extends Application {

    Button signinButton, signupButton, signoutButton, signinRedirectButton, signupRedirectButton, forgotPasswordButton, helpButton, endProgramButton;
    HBox signinEmailContainer, signinPasswordContainer, signupEmailContainer, signupPasswordContainer, signupPasswordConfirmationContainer;
    VBox signinLayout, signupLayout, deviceIndexLayout;
    Scene signinScene, signupScene, deviceIndexScene;
    Text signinHeader, signupHeader, deviceIndexHeader;
    Label signinEmailLabel, signinPasswordLabel, signupEmailLabel, signupPasswordLabel, signupPasswordConfirmationLabel;
    // The static keyword denotes that a member variable, or method, can be accessed without
    // requiring an instantiation of the class to which it belongs.
    static TextField signinEmailTextField, signinPasswordTextField, signupEmailTextField, signupPasswordTextField, signupPasswordConfirmationTextField;

    @Override
    public void start(Stage primaryStage) throws Exception{
        signinHeader = new Text();
        signinHeader.setFont(new Font(20));
        signinHeader.setText("Waggle: Sign In");

        signupHeader = new Text();
        signupHeader.setFont(new Font(20));
        signupHeader.setText("Waggle: Sign Up");

        deviceIndexHeader = new Text();
        deviceIndexHeader.setFont(new Font(20));
        deviceIndexHeader.setText("Waggle: Your Devices");

        // a label is just static text, the user cannot really interact
        // with a label in any way
        signinEmailLabel = new Label();
        signinEmailLabel.setText("Email");
        signinEmailTextField = new TextField();
        signinEmailTextField.setPromptText("Enter email here");
        signinEmailContainer = new HBox();
        signinEmailContainer.getChildren().addAll(signinEmailLabel, signinEmailTextField);
        signinEmailContainer.setSpacing(10);
        signinEmailContainer.setAlignment(Pos.CENTER);

        signinPasswordLabel = new Label();
        signinPasswordLabel.setText("Password");
        signinPasswordTextField = new TextField();
        signinPasswordTextField.setPromptText("Enter password here");
        signinPasswordContainer = new HBox();
        signinPasswordContainer.getChildren().addAll(signinPasswordLabel, signinPasswordTextField);
        signinPasswordContainer.setSpacing(10);
        signinPasswordContainer.setAlignment(Pos.CENTER);

        signupEmailLabel = new Label();
        signupEmailLabel.setText("Email");
        signupEmailTextField = new TextField();
        signupEmailTextField.setPromptText("Enter your email here");
        signupEmailContainer = new HBox();
        signupEmailContainer.getChildren().addAll(signupEmailLabel, signupEmailTextField);
        signupEmailContainer.setSpacing(10);
        signupEmailContainer.setAlignment(Pos.CENTER);

        signupPasswordLabel = new Label();
        signupPasswordLabel.setText("Password");
        signupPasswordTextField = new TextField();
        signupPasswordTextField.setPromptText("Enter your password here");
        signupPasswordContainer = new HBox();
        signupPasswordContainer.getChildren().addAll(signupPasswordLabel, signupPasswordTextField);
        signupPasswordContainer.setSpacing(10);
        signupPasswordContainer.setAlignment(Pos.CENTER);

        signupPasswordConfirmationLabel = new Label();
        signupPasswordConfirmationLabel.setText("Password Confirmation");
        signupPasswordConfirmationTextField = new TextField();
        signupPasswordConfirmationTextField.setPromptText("Enter your password again here");
        signupPasswordConfirmationContainer = new HBox();
        signupPasswordConfirmationContainer.getChildren().addAll(signupPasswordConfirmationLabel, signupPasswordConfirmationTextField);
        signupPasswordConfirmationContainer.setSpacing(10);
        signupPasswordConfirmationContainer.setAlignment(Pos.CENTER);

        signinButton = new Button();
        signinButton.setText("Sign In");
        // `this` declares that the code to handle this button click is located
        // in this Class
        signinButton.setOnAction(e -> handleSigninButtonClick(primaryStage, deviceIndexScene));

        signupButton = new Button();
        signupButton.setText("Sign Up");
        signupButton.setOnAction(e -> handleSignupButtonClick(primaryStage, deviceIndexScene));

        signinRedirectButton = new Button();
        signinRedirectButton.setText("I have an account, sign me in");
        signinRedirectButton.setOnAction(e -> primaryStage.setScene(signinScene));

        signupRedirectButton = new Button();
        signupRedirectButton.setText("Create an account");
        signupRedirectButton.setOnAction(e -> primaryStage.setScene(signupScene));

        forgotPasswordButton = new Button();
        forgotPasswordButton.setText("Forgot your password?");
        // here is an example of an anonymous inner class
        // can be used for making more compact code
        forgotPasswordButton.setOnAction(e -> handleForgotPasswordButtonClick());

        helpButton = new Button();
        helpButton.setText("Need help?");
        // here is an example of a Java 8 lambda function
        // this is much more compact than an anonymous class
        // Java is able to infer that e should be treated as our EventHandler
        helpButton.setOnAction(e -> getHostServices().showDocument("https://www.gowaggle.io/"));

        signoutButton = new Button();
        signoutButton.setText("Sign Out");
        signoutButton.setOnAction(e -> {
            String title = "Are you sure you want to sign out?";
            String subtitle = "Any unsaved changes will be lost.";
            if (ConfirmAlert.display(title, subtitle)) {
                primaryStage.setScene(signinScene);
            }
        });

        endProgramButton = new Button();
        endProgramButton.setText("Close Program");
        endProgramButton.setOnAction(e -> {
            // consume() tells java that we will handle program execution from here
            // prevents auto close of the window
            e.consume();
            handleEndProgram(primaryStage);
        });
        primaryStage.setOnCloseRequest(e -> {
            e.consume();
            handleEndProgram(primaryStage);
        });

        signinLayout = new VBox();
        signinLayout.setSpacing(5.00);
        signinLayout.getChildren().add(signinHeader);
        signinLayout.getChildren().add(signinEmailContainer);
        signinLayout.getChildren().add(signinPasswordContainer);
        signinLayout.getChildren().add(signinButton);
        signinLayout.getChildren().add(signupRedirectButton);
        signinLayout.getChildren().add(forgotPasswordButton);
        signinLayout.getChildren().add(helpButton);
        signinLayout.getChildren().add(endProgramButton);
        signinLayout.setAlignment(Pos.CENTER);
        signinLayout.setFillWidth(true);

        signupLayout = new VBox();
        signupLayout.setSpacing(5.00);
        signupLayout.getChildren().add(signupHeader);
        signupLayout.getChildren().add(signupEmailContainer);
        signupLayout.getChildren().add(signupPasswordContainer);
        signupLayout.getChildren().add(signupPasswordConfirmationContainer);
        signupLayout.getChildren().add(signupButton);
        signupLayout.getChildren().add(signinRedirectButton);

        deviceIndexLayout = new VBox();
        deviceIndexLayout.setSpacing(5.00);
        deviceIndexLayout.getChildren().add(deviceIndexHeader);
        deviceIndexLayout.getChildren().add(signoutButton);

        signinScene = new Scene(signinLayout, 600, 600);
        signupScene = new Scene(signupLayout, 600, 600);
        deviceIndexScene = new Scene(deviceIndexLayout, 600, 600);

        primaryStage.setTitle("Waggle: 3D Printing Made Easy");
        // set the default scene here
        primaryStage.setScene(signinScene);

        primaryStage.show();
    }

    public static void handleEndProgram(Stage primaryStage) {
        String title = "Are you sure you want to exit the program?";
        String subtitle = "Any unsaved changes will be lost.";
        if (ConfirmAlert.display(title, subtitle)) {
            primaryStage.close();
        }
    }

    public static void handleSigninButtonClick(Stage primaryStage, Scene deviceIndexScene) {
        System.out.println("Sign in button was clicked!");
        // remove all whitespace from email and password
        String email = signinEmailTextField.getText().replaceAll("\\s+","");
        String password = signinPasswordTextField.getText().replaceAll("\\s+","");
        if (isNotNullEmptyString(email) && isNotNullEmptyString(password)) {
            primaryStage.setScene(deviceIndexScene);
        } else if (isNotNullEmptyString(email)) {
            WarningAlert.display("Missing Password", "Please do not forget your password");
        } else if (isNotNullEmptyString(password)) {
            WarningAlert.display("Missing Email", "Please do not forget your email");
        } else {
            WarningAlert.display("Missing Information", "Please do not forget your email and password");
        }
    }

    public static void handleSignupButtonClick(Stage primaryStage, Scene deviceIndexScene) {
        System.out.println("Wow, Sign up button was clicked!!!");
        primaryStage.setScene(deviceIndexScene);
    }

    public static void handleForgotPasswordButtonClick() {
        System.out.println("FORGOT PASSWORD BUTTON WAS CLICKED");
    }

    public static Boolean isNotNullEmptyString(String target) {
        return target != null && !target.isEmpty();
    }

    // the launch command makes our program go into the Application class,
    // set everything up, and then it will call the `start` command (above)
    public static void main(String[] args) {
        launch(args);
    }
}

// the actual frame of the javafx application is called the stage
// everything inside the stage is called, the scene
