package sample;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class WarningAlert {
    public static void display(String title, String message) {
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(title);
        window.setMinHeight(200);
        window.setMinWidth(200);

        Label warningLabel = new Label();
        warningLabel.setText(message);

        Button okButton = new Button();
        okButton.setText("OK");
        okButton.setOnAction(e -> window.close());

        VBox warningLayout = new VBox();
        warningLayout.setSpacing(10);
        warningLayout.getChildren().addAll(warningLabel, okButton);

        Scene warningScene = new Scene(warningLayout);

        window.setScene(warningScene);

        window.showAndWait();
    }
}
